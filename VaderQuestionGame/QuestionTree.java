//Jiangzhenjun Liao 1429888
//CSE 143, AI Jeff, HW1
//May 20, 2016
import java.util.Scanner;
import java.io.PrintStream;

// this class is used in playing a question game with the user
// by gussing the object in the player's mind while the player
// will answer yes/no questions provided by the computer
public class QuestionTree {
   private QuestionNode topNode;
   private Scanner console;
   private UserInterface ui;
   private int countWon;
   private int countTotal;
   
   // pre: the passed in UserInterface should not be null
   // otherwise throws an IllegalArgumentException.
   // pre: there is only one answer "computer" to begin with
   // Constructor inicializes a new game with given user interface  
   public QuestionTree(UserInterface ui) {
      if (ui == null) {
         throw new IllegalArgumentException();
      }
      this.countWon = 0;
      this.countTotal = 0;
      this.ui = ui;
      this.topNode = new QuestionNode("computer", null, null);
   }
      
   // plays one complete game with the user by asking yes/no questions
   // until reaching the expecting object. 
   // updates the number of game played
   // If gussed the correct answer, the computer would say it wins,
   // otherwise it will ask the user to add information
   public void play() {
      this.countTotal++; // one game is played
      this.topNode = this.play(this.ui, this.topNode);
   }
   
   // takes in the UserInterface and the last guess 
   // for reciving the expecting object's name,
   // a y/n question that distinguish the object from last guess,
   // and the player's y/n answer for the question.
   // returns the improved node after one game is played
   private QuestionNode play(UserInterface ui, QuestionNode topNode) {
      if (topNode.isAnswer()) { // at the end of the tree
         ui.print("Would your object happen to be " + topNode.question + "?");
         if(this.ui.nextBoolean()) { // if the answer is yes
            this.ui.println("I win!");
            this.countWon++;
         } else { // if the answer is wrong
            topNode = addNew(ui, topNode); // we don't have it! let's add it!
         }            
      }else { // it is not the end of the tree
         ui.print(topNode.message());
         if(ui.nextBoolean()) { // if the answer is yes
            topNode.yes = play(ui, topNode.yes); // go to the yes branch
         }else { // the answer is no
            topNode.no = play(ui, topNode.no); // go to the no branch
         }
      }
      return topNode;
   }
   
   // takes in the user interface for recieving information and ask questions
   // adds in a new node that's not in the file
   // returns the improved tree
   private QuestionNode addNew(UserInterface ui, QuestionNode current) {
      ui.print("I lose. What is your object?");
      String object = ui.nextLine();
      ui.print("Type a yes/no question to distinguish your item from " 
                           + current.message() + ":");
      String question = ui.nextLine();
      ui.print("And what is the answer for your object?");
      QuestionNode temp = current;
      current = new QuestionNode(question);
      if (ui.nextBoolean()) { // the answe is yes
         current.yes = new QuestionNode(object); // put the object to the yes branch
         current.no = temp;// the last guess goes to the no branch
      } else {
         current.yes = temp;
         current.no = new QuestionNode(object);
      }
      return current;
   }
   
   // takes in a printstream to print the current tree state to an output file
   // the file is in pre-order traversal, begin with Q or A to indicate whether
   // the line is a question or an answer.
   // pre: the passed in PrintStream should not be null
   // otherwise throws an IllegalArgumentException.
   public void save(PrintStream output) {
      if (output == null) {
         throw new IllegalArgumentException();
      }
      printPreorder(output, this.topNode);
   }
   
   // prints out the questions and answers of the tree
   private void printPreorder(PrintStream output, QuestionNode topNode) {
      if (!topNode.isAnswer()) {
         output.println("Q:" + topNode.question);
         printPreorder(output, topNode.yes);
         printPreorder(output, topNode.no);
      } else {
         output.println("A:" + topNode.message());  
      }
   }
   
   // takes in a Scanner to read a file to replaces the current tree
   // pre: the passed in Scanner should not be null
   // otherwise throws an IllegalArgumentException.
   public void load(Scanner input) {
      if (input == null) {
         throw new IllegalArgumentException();
      }
      this.topNode = this.load(input, this.topNode);
   }
   
   // takes in a scanner to read the file and returns the replaced tree
   private QuestionNode load(Scanner input, QuestionNode current ) {
      String first = input.nextLine();
      String text = first.substring(2); // elimiating Q or A at th beginning
      current = new QuestionNode(text);
      if(!current.isAnswer() || first.startsWith("Q")) {
         current.yes = this.load(input, current.yes);
         current.no = this.load(input, current.no);   
      }
      return current;
   }
   
   // returns the total number of games that has been played by the user
   public int totalGames() {
      return this.countTotal;
   }
   
   // returns the total number of games that has won by the computer
   public int gamesWon() {
      return this.countWon;
   }
}