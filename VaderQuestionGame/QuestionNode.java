//Jiangzhenjun Liao 1429888
//CSE 143, AI Jeff, HW1
//May 20, 2016

// Class QuestionNode is used to provide nodes for class QuestionTree
public class QuestionNode {
   public String question;
   public QuestionNode yes; 
   public QuestionNode no; 
   
   // construct a new node by taking in the y/n question
   public QuestionNode(String question) {
      this(question, null, null);
   }

   // takes in the y/n question, the yes branch, and the no branch
   // construct a new node
   public QuestionNode(String question, QuestionNode yes, QuestionNode no) {
      this.question = question;
      this.yes = yes;       
      this.no = no;
   }
   
   // return the string form of the current node
   public String message() {
      return this.question;
   } 
   
   // return if the current node is the end of the tree
   public boolean isAnswer() {
      return (this.yes == null && this.no == null);
   }
   
   
}