# Project Overview
In this project I implement a yes/no guessing game called "20 Questions." Each round of the game begins by the human player thinking of an object. The computer will try to guess your object by asking you a series of yes or no questions. Eventually the computer will have asked enough questions that it thinks it knows what object you are thinking of. It will make a guess about what your object is. If this guess is correct, the computer wins; if not, you win.

## What did I do?
In this project, the computer keeps track of a binary tree whose nodes represent questions and answers. I create classes QuestionTree and QuestionNode to represent the computer's tree of yes/no questions and answers for playing games of 20 Questions. I was provided with a client QuestionMain that handles user interaction and calls my tree's methods to play games.

## Project Language and Structure
* Java
* Binary Search Tree

## How to play it?
Please download all the files on your local machine. Run *VaderMain.class* on a your local machine and start playing! :video_game:

*You can choose to paly with loading previous games by enter *memory.txt* at the beginning or save your own quesions and answers. *