<?php
/**
 * HW8 Jiangzhenjun liao
 * Student#: 1429888
 * CSE 154, TA: Susan Wolfgram
 * This is a PHP service that will provide the initial welcome page for users
 */
    include ("common.php"); // implements the shared code php file

    top(); ?> <!--gets the common top content from the common.php-->

    <!--prints the descriptive headers for starting page-->
    <h1>The One Degree of Kevin Bacon</h1>
    <p>Type in an actor's name to see if he/she was ever in a movie with Kevin Bacon!</p>
    <p><img src="https://webster.cs.washington.edu/images/kevinbacon/kevin_bacon.jpg"
            alt="Kevin Bacon" /></p>
    <?php

    bottom(); // gets the common bottom content from the common.php

?>

