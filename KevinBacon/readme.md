# Project Overview
The Six Degrees of Kevin Bacon is a game based upon the theory that every actor can be connected to actor Kevin Bacon by a chain of movies no more than 6 in length. Most, but not all, can reach him in 6 steps. 12% of all actors
cannot reach him at all. My site shows the movies in which another actor has appeared with Kevin Bacon. The site also shows a list of all movies in which the other actor has appeared. :tada:

## What did I do?
This project focuses on querying a relational database using SQL in PHP. I write the HTML/CSS and PHP code for a web site called MyMDb that mimics part of the popular IMDb movie database site.

## Project Language and Structure
* SQL
* PHP
* HTML
* CSS

## How to view it?
Please download all the files on your local machine. Run *mymdb.php* on a server and start searching for movies by actors or finding out their relationships with Kevin Bacon!

## More to read on Kevin Bacon
[Wikipedia - Six Degrees of Kevin Bacon](https://en.wikipedia.org/wiki/Six_Degrees_of_Kevin_Bacon)

[Oracle of Bacon link](http://oracleofbacon.org/) - a real implementation of the Six Degrees of Kevin Bacon

[Oracle of Bacon - stats on possible "center" actors](http://oracleofbacon.org/center_list.php)
