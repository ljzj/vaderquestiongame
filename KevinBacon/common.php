<?php
/*
 * HW7 Jiangzhenjun liao
 * Student#: 1429888
 * CSE 154, TA: Susan Wolfgram
 * This is a PHP service that will provide shared codes between all pages
 */

    // provides the common top content that shares to all pages
    function top() { ?>
        <!DOCTYPE html>
        <html>
        <head>
            <title>My Movie Database (MyMDb)</title>
            <meta charset="utf-8" />
            <link href="https://webster.cs.washington.edu/images/kevinbacon/favicon.png"
                  type="image/png" rel="shortcut icon" />

            <!-- Link to CSS file -->
            <link href="bacon.css" type="text/css" rel="stylesheet" />
        </head>

        <body>
        <div id="frame">
            <div id="banner">
                <a href="mymdb.php">
                    <img src="https://webster.cs.washington.edu/images/kevinbacon/mymdb.png"
                         alt="banner logo" /></a>
                My Movie Database
            </div>

        <div id="main">
        <?php
    }

    // creates new PDO objects and gets data on the imdb database
    $db = new PDO("mysql:dbname=imdb;host=localhost;charset=utf8", "ljzj", "O2RWVArDTU");

    // gets the input first name and last name from user
    $inputfirstname = $_GET["firstname"];
    $inputlastname = $_GET["lastname"];

    // gets the id from the given actor and returns it
    //  takes in the database, input first name and input last name as parameters
    function getID($db, $inputfirstname, $inputlastname) {

        // finds the id for a given actor's name that matches the exact last name
        // and starts with the first name
        // picks only one result from results ordered by year descending and movie title ascending
        $id = $db->query("SELECT id, film_count 
                          FROM actors 
                          WHERE last_name = '$inputlastname'
                          AND first_name LIKE '$inputfirstname%' 
                          ORDER BY film_count DESC, id ASC Limit 1");
        return $id;
    }

    // prints the html result table
    // takes in rows as a parameter
    function printTable($rows) {
        ?>
        <table>
            <tr>
                <th>#</th>
                <th>Title</th>
                <th>Year</th>
            </tr>
            <?php
            $i = 1; // counts the row
            foreach ($rows as $row) {
                ?>
                <tr>
                    <td><?= $i ?></td>
                    <td><?= $row["name"] ?></td>
                    <td><?= $row["year"] ?></td>
                </tr>
                <?php
                $i++;
            } ?>
        </table>
        <?php
    }

    // provides the bottom shared content for all pages
    function bottom(){ ?>
                <!-- form to search for every movie by a given actor -->
                <form action="search-all.php" method="get">
                    <fieldset>
                        <legend>All movies</legend>
                        <div>
                            <input name="firstname" type="text" size="12"
                                   placeholder="first name" autofocus="autofocus" />
                            <input name="lastname" type="text" size="12"
                                   placeholder="last name" />
                            <input type="submit" value="go" />
                        </div>
                    </fieldset>
                </form>

                <!-- form to search for movies where a given actor was with Kevin Bacon -->
                <form action="search-kevin.php" method="get">
                    <fieldset>
                        <legend>Movies with Kevin Bacon</legend>
                        <div>
                            <input name="firstname" type="text" size="12" placeholder="first name" />
                            <input name="lastname" type="text" size="12" placeholder="last name" />
                            <input type="submit" value="go" />
                        </div>
                    </fieldset>
                </form>
            </div> <!-- end of #main div -->

            <div id="w3c">
                <a href="https://webster.cs.washington.edu/validate-html.php">
                    <img src="https://webster.cs.washington.edu/images/w3c-html.png"
                         alt="Valid HTML5" /></a>
                <a href="https://webster.cs.washington.edu/validate-css.php">
                    <img src="https://webster.cs.washington.edu/images/w3c-css.png"
                         alt="Valid CSS" /></a>
            </div>
        </div> <!-- end of #frame div -->
        </body>
        </html>
    <?php
    }
?>
