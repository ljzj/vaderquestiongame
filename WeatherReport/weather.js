/**
 * Jiangzhenjun Liao
 * Student ID: 1429888
 * Section: AE
 * Assignment: 5
 * This is a javascript file for weather page;
 * It helps generate current and future weather information of a given city.
 */

(function() {
    "use strict";

    // Stores the target city's current day temperatures and it's description
    var tempsAndDescription = [];

    // Attaches handlers to the buttons and hides the precipitation part.
    // Uses a helper method to load list of cities from the server
    window.onload = function() {
        // Uses a helper method to fetch a request of loading data from the given server
        helper(loadCities, "https://webster.cs.washington.edu/cse154/weather.php?mode=cities", "");
        document.getElementById("graph").style.display = "none";
        document.getElementById("search").onclick = search;
        document.getElementById("slider").onchange = changeSlider;
        document.getElementById("precip").onclick = precipitation;
        document.getElementById("temp").onclick = showTemp;
    };

    // Called by the helper method when the Ajax text arrived
    // Creates a list of options below the input box, one for each city.
    function loadCities() {
        var cities = this.responseText; // grab the text
        var eachCity = cities.split(/[ \t\n]+/); // split every city's name
        for (var i = 0; i < eachCity.length; i++) {
            // Makes each city an option for user to choose
            var option = document.createElement("option");
            option.type = "option";
            option.value = eachCity[i];
            document.getElementById("cities").appendChild(option);
        }
        // When the list finishes loading, hides the loading image
        document.getElementById("loadingnames").style.display = "none";
    }

    // Called when the search button is clicked
    // Searches the weather for the target city given by the user
    function search() {
        // Shows the result area, resets the previous appear or disappear settings
        resetPage();
        // Gets the given input city's name
        var target = document.getElementById("citiesinput").value;
        // Uses a helper method to load data of current day weather of target city from server
        helper(loadDay,
            "https://webster.cs.washington.edu/cse154/weather.php?mode=oneday&city=", target);
        // Uses a helper method to load data of future days' weather of target city from server
        helper(loadWeek,
            "https://webster.cs.washington.edu/cse154/weather.php?mode=week&city=", target);
    }

    // called by the helper method when the XML data of current day weather arrives
    // Inserts new paragraph to the html file for city name, date and the
    // description of the weather. Creates a precipitation table
    function loadDay() {
        if (this.status == 200) { // request is succeed
            var response = this.responseXML;
            // Initiates empty arrays to handel information
            var temps = [];
            var descriptions = [];
            var chances = [];

            // stores Nodelists of temperatures, descriptions, and clouds chances in new arrays
            var temp = response.querySelectorAll("temperature");
            var symbols = response.querySelectorAll("symbol");
            var cloud = response.querySelectorAll("clouds");

            // get information from each nodelist and handles and stores them in empty arrays
            for (var i = 0; i < temp.length; i++) {
                temps[i] = Math.round(temp[i].textContent); // round the temperature
                descriptions[i] = symbols[i].getAttribute("description"); // get the description
                chances[i] = cloud[i].getAttribute("chance"); // get the cloud chance
                // stores temperatures and descriptions into the module variable
                tempsAndDescription[i * 2] = temps[i];
                tempsAndDescription[i * 2 + 1] = descriptions[i];
            }

            // creates paragraph div for city name
            var city = document.createElement("p");
            city.className = "title"; // gives it a class and gets the city name
            city.innerHTML = response.getElementsByTagName("name")[0].firstChild.nodeValue;
            document.getElementById("location").appendChild(city); // append it to location div

            // creates paragraph div for current date
            var date = document.createElement("p");
            date.innerHTML = Date(); // prints current date and time
            document.getElementById("location").appendChild(date);

            // creates paragraph div for current weather description
            var description = document.createElement("p");
            description.id = "description";
            description.innerHTML = descriptions[0];
            document.getElementById("location").appendChild(description);
            // shows the current temperature in the given div
            document.getElementById("currentTemp").innerHTML = temps[0] + "&#8457";

            // when location information finishes loading, hides the loading image
            document.getElementById("loadinglocation").style.display = "none";

            // Takes chances as a parameter to create precipitation table in the graph div
            // Initially hidden until the user clicks the precipitation button
            createGraph(chances);

            // sets buttons and slider to be visible
            document.getElementById("buttons").style.display = "block";
            document.getElementById("slider").style.display = "block";

        } else if (this.status !== 410) { // error that is not missing data
            // shows up an error message
            document.getElementById("errors").innerHTML = "Hey, Something goes wrong!!!";
        } else { // missing data
            // calls an error method
            error();
        }
    }

    // called when the slider's position is changed by the user
    // each position corresponds to a temperature  and a description of a specific time
    function changeSlider(){
        var position = document.getElementById("slider").value;
        document.getElementById("currentTemp").innerHTML
            = tempsAndDescription[position / 3 * 2] + "&#8457"; //temperature
        document.getElementById("description").innerHTML
            = tempsAndDescription[position / 3 * 2 + 1]; //description
    }

    // called by the helper method when the JSON data of future days' weather arrives
    // creates a two rows table with first rows displaying the weather icon and the second
    // row showing the temperature associated with the weather icon
    function loadWeek() {
        if (this.status == 200) { // request is succeed
            var response = JSON.parse(this.responseText);
            var table = document.getElementById("forecast");
            var row1 = document.createElement("tr"); // creates first row
            var row2 = document.createElement("tr"); // creates second row
            // goes through each temperature and icon data
            for (var i = 0; i < response.weather.length; i++) {
                // deals with each image icon
                var icons = document.createElement("td");
                var img = document.createElement("img");
                img.src = "https://openweathermap.org/img/w/" + response.weather[i].icon + ".png";
                icons.appendChild(img);
                row1.appendChild(icons);
                // deals with each temperature data
                var temps = document.createElement("td");
                var temp = document.createElement("div");
                temp.innerHTML = Math.round(response.weather[i].temperature) + "&#176";
                temps.appendChild(temp);
                row2.appendChild(temps);
            }
            // append two rows to the table
            table.appendChild(row1);
            table.appendChild(row2);
            // when the forecast table finished loading, hides the loading image
            document.getElementById("loadingforecast").style.display = "none";
        } else if (this.status !== 410) { // error is not missing data
            // shows up a error message
            document.getElementById("errors").innerHTML = "Hey, Something goes wrong!!!";
        } else { // missing data
            // calls an error method
            error();
        }
    }

    // called when the precipitation button is clicked
    // shows the precipitation table, hides the slider
    function precipitation() {
        document.getElementById("temps").style.display = "none";
        document.getElementById("graph").style.display = "block";
    }

    // called when the temperature button is clicked
    // shows the slider, hides the precipitation table
    function showTemp() {
        document.getElementById("temps").style.display = "block";
        document.getElementById("graph").style.display = "none";
    }

    // A helper method which makes ajax request to fetch data from the server
    // takes in a function name, a web server link and a target as parameters
    function helper (functionName, link, target) {
        var xhr = new XMLHttpRequest();
        xhr.onload = functionName;
        xhr.open("GET", link + target, true);
        xhr.send();
    }

    // Takes a parameter "chance" to fill the table with one row of precipitation bars
    function createGraph(chances) {
        var table = document.getElementById("graph");
        var row = document.createElement("tr");
        // goes through each chance data
        for(var i = 0; i < chances.length; i++) {
            var td = document.createElement("td");
            var div = document.createElement("div");
            div.innerHTML = chances[i] + "%";
            // sets each bar's height to pixels as how likely it is to rain in percentages
            div.style.height = chances[i] + "px";
            td.appendChild(div);
            row.appendChild(td);
        }
        table.appendChild(row);
        // When the graph finishes loading, hides the loading image
        document.getElementById("loadinggraph").style.display = "none";
    }

    // called when the user clicked the search button
    // Initiates/resets hiding/showing properties
    function resetPage() {
        document.getElementById("slider").value = 0; // sets slider's position to zero
        document.getElementById("location").innerHTML = ""; // clears previous location info
        document.getElementById("graph").innerHTML = ""; // clears previous graph table
        document.getElementById("forecast").innerHTML = ""; // clears previous forecast table
        // shows loading images for each section
        document.getElementById("loadinglocation").style.display = "block";
        document.getElementById("loadinggraph").style.display = "block";
        document.getElementById("loadingforecast").style.display = "block";
        document.getElementById("resultsarea").style.display = "block"; //shows the result area
        document.getElementById("buttons").style.display = "none"; // hides buttons
        document.getElementById("slider").style.display = "none"; // hides the slider
        document.getElementById("nodata").style.display = "none"; // hides the missing data notice
    }

    // called when there is a missing data error, status == 410
    // hides all loading images and shows the no-data message
    function error() {
        document.getElementById("loadinglocation").style.display = "none";
        document.getElementById("loadinggraph").style.display = "none";
        document.getElementById("loadingforecast").style.display = "none";
        document.getElementById("nodata").style.display = "block";
    }

})();