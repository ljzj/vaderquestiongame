# Project Overview
When you Google “weather” you will see some information much like the screenshots above. Multiple places keep track of weather data like this including OpenWeatherMap. OpenWeatherMap provides data in both XML and JSON formats. This project displays weather data on a website similarly to how Google does.

## What did I do?
I used Ajax to fetch data in text, XML, and JSON formats.

## Project Language and Structure
* JSON
* CSS
* HTML

## How to view it?
Please download all the files on your local machine. Open *weather.html* and start searching for weathers and forcasts! :sunny: :umbrella:

