/**
 * Jiangzhenjun Liao
 * Student ID: 1429888
 * Section: AE
 * Assignment: 3
 * This is a javascript file for speedReader page (no extra feature included);
 * It helps users to read faster by using method called Rapid Serial Visual Presentation.
 */

(function() {
    'use strict';

    var timer = null;
    var countUp = 0;
    var speed = 171; // default speed
    var result = "";

    window.onload = function() {
        // attaches handler functions to buttons
        document.getElementById("start").onclick = start;
        document.getElementById("stop").onclick = stop;
        document.getElementById("medium").onchange = changeSize;
        document.getElementById("big").onchange = changeSize;
        document.getElementById("bigger").onchange = changeSize;
        document.getElementById("speed").onchange = changeSpeed;

    };

    // Start word display animation of what user has typed in the text area.
    function start() {
        // Animation is in progress, Stop button enabled
        // the Start button and the text area are disabled
        document.getElementById("stop").disabled = false;
        document.getElementById("start").disabled = true;
        document.getElementById("textArea").disabled = true;

        // get the input from text area and split by space and break, returns an array
        var input = document.getElementById("textArea").value.split(/[ \t\n]+/);

        // go through each word in the array
        for (var i = 0; i < input.length; i++) {
            if (input[i].endsWith(",") || input[i].endsWith(".") ||
                input[i].endsWith("!") || input[i].endsWith("?") ||
                input[i].endsWith(";") || input[i].endsWith(":")) { // word ends with punctuation
                // remove the last piece of punctuation
                input[i] = input[i].substring(0, input[i].length - 1);
                // add the current word twice
                result += input[i] + " " + input[i] + " ";
            } else { // word without punctuation
                result += input[i] + " ";
            }
        }

        // stops the timer from restarting if it is already running.
        if (timer === null) {
            timer = setInterval(delayedWord, speed);
        }
    }


    // Stops current display of animation and resets the timer and current word position.
    function stop() {
        // Animation is not in progress, Stop button disabled
        // the Start button and the text area are enabled.
        document.getElementById("start").disabled = false;
        document.getElementById("textArea").disabled = false;
        clearInterval(timer);
        document.getElementById("display").innerHTML = "";
        timer = null;
        countUp = 0;
    }

    // Display single frame of animation at selected speed.
    // If reaches the end, resets the timer.
   function delayedWord() {
       var output = result.split(/[ \t\n]+/);
       document.getElementById("display").innerHTML = output[countUp];
       if (output.length == countUp + 1) { // reaches the end
           clearInterval(timer); // clears the timer
           timer = null; // resets timer
       } else {
           countUp++;
       }
    }

    // Changes the font of words displaying in progress depends on user's preference.
    function changeSize() {
        // gets the current displaying word and changes its size to currently selected font
        document.getElementById("display").style.fontSize = this.value + "pt";
    }

    // Changes the speed of the animation depends on user's preference.
    function changeSpeed() {
        if (timer !== null) {
            clearInterval(timer);
            speed = document.getElementById("speed").value;
            timer = setInterval(delayedWord, speed);
        }
    }

})();