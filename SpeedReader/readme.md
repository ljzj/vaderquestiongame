# Project Overview
Everyone wishes they could read faster. One method for doing so is called Rapid Serial Visual Presentation (RSVP).

RSVP is based on three observations:
* Using your finger or some other pointing device to train your eyes and focus while reading increases speed.
* Eliminating subvocalization, internally speaking words while reading them, can dramatically increase your reading speed.
*Speed reading skills rely on the reader’s discipline to develop good reading habits, and it is easy for a reader to learn "the wrong way" and thus never see the purported benefits of speed reading.

Computer programs are a great help in this area as they force readers to accurately do one and two while avoiding three. RSVP programs do this by presenting words to the reader in quick succession. Therefore, the reader is only able to focus on a single word at a time. And furthermore, the words appear at such a speed that the reader is unable to subvocalize like normal.  

## What did I do?
This project shows my understanding of JavaScript and its interaction with HTML user interfaces.

## Project Language and Structure
* Javascript
* HTML user interface

## How to run it?
Please download all the files on your local machine. Open *speedreader.html* and start building your reading habits! :eyes:

There are some options that you can choose:
* Font size
* Reading Speed
* Text input
