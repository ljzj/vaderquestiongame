# Welcome to Vera Liao's project page!
Hello there, welcome!

## Introduction
In this page, you will find all the projects that I have done. For each project, there will be instructions that will get you a copy of the porject up and running on your local machine for developing and testing purposes. Plase click one project folder and see detialed descriptions of each projects and how to run them. 

Please enjoy and I hope you have fun！

## Projects Overview

### Vader Question Game :space_invader:
A user interaction game with graphic user interface that implements a yes/no guessing game called "20 Questions." Each round of the game begins by the human player thinking of an object. The computer will try to guess your object by asking you a series of yes or no questions. Eventually the computer will have asked enough questions that it thinks it knows what object you are thinking of. It will make a guess about what your object is. If this guess is correct, the computer wins; if not, you win.

### Kevin Bacon :movie_camera:
The Six Degrees of Kevin Bacon is a game based upon the theory that every actor can be connected to actor Kevin Bacon by a chain of movies no more than 6 in length. Most, but not all, can reach him in 6 steps. 12% of all actors
cannot reach him at all. My site shows the movies in which another actor has appeared with Kevin Bacon. The site also shows a list of all movies in which the other actor has appeared. 

### Fifteen Puzzle :question:
The Fifteen Puzzle is a simple classic game consisting of a 4x4 grid of numbered squares with one square missing. The object of the game is to arrange the tiles into numerical order by repeatedly sliding a square that neighbors the missing square into its empty space. To play this game, users can shuffle the grids and solve them through clicking on individual grids. 

### To Do List :books:
I create a web page for a fictional online to-do list site called "Remember the Cow", which is a parody of the real to-do list web site "Remember the Milk".

### Weather Report :sunny:
When you Google “weather” you will see some information much like the screenshots above. Multiple places keep track of weather data like this including OpenWeatherMap. OpenWeatherMap provides data in both XML and JSON formats. This project displays weather data on a website similarly to how Google does.

### Speed Reader :eyes:
Everyone wishes they could read faster. One method for doing so is called Rapid Serial Visual Presentation (RSVP).This projects helps present words to the reader in quick succession. Therefore, the reader is only able to focus on a single word at a time. And furthermore, the words appear at such a speed that the reader is unable to subvocalize like normal.  

### Best Reads :scissors:
This is a mock book-review website where you can find ratings and readers' reviews of different books. 

### Movie Website Mock Up :clapper:
This is a mock move-review website where you can find ratings and audiences' reviews of a fake movie. 

## Main Language Used
* Java
* Javascript
* CSS
* HTML
* PHP

## Project Source
University of Washington CS Courses

