<?php
/*
 * HW8 Jiangzhenjun liao
 * Student#: 1429888
 * CSE 154, TA: Susan Wolfgram
 * This is a PHP service page which shows the user's to-do list
 * and lets them add/delete items from it
*/
    include_once ("common.php"); // implements a shared code file
    notLoggedIn(); // if user has not been logged in, directs to start.php
    top(); // gets the common top content from common.php
    ?>

    <div id="main">
        <h2> <?= $_SESSION["name"]?> 's To-Do List</h2> <!--shows the user's name-->

        <ul id="todolist">
            <?php
            // if current user's todolist file exist
            if (file_exists("todo_" . $_SESSION["name"] . ".txt")) {
                $index = 0; // sets index
                $lines = file("todo_" . $_SESSION["name"] . ".txt"); // sores each line
                // goes through items in file, creates a list element for each of them
                foreach($lines as $line) { ?>
                    <li>
                        <form action="submit.php" method="post">
                            <input type="hidden" name="action" value="delete" />
                            <input type="hidden" name="index" value="<?= $index ?>" />
                            <input type="submit" value="Delete" />
                        </form>
                        <?= htmlspecialchars($line) ?> <!--html encode-->
                    </li>
                    <?php
                    $index++; // updates index
                }
            } ?>
                <li>
                    <form action="submit.php" method="post">
                        <input type="hidden" name="action" value="add" />
                        <input name="item" type="text" size="25" autofocus="autofocus" />
                        <input type="submit" value="Add" />
                    </form>
                </li>
            </ul>

        <div>
            <a href="logout.php"><strong>Log Out</strong></a>
            <em>(logged in since <?= $_COOKIE["logintime"]?>)</em>
        </div>

    </div>

    <?php bottom(); // gets the common bottom content from common.php

?>