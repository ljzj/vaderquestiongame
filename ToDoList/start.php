<?php
/**
 * HW8 Jiangzhenjun liao
 * Student#: 1429888
 * CSE 154, TA: Susan Wolfgram
 * This is a PHP which provides initial web page describing the site
 * with a form for the user to log in or register
 */
    include("common.php"); // implements a shared code file
    hasLoggedIn(); // if the user has already logged in, redirect to todolist.php
    top(); //  gets the common top content from common.php
    ?>

    <div id="main">
        <p>
            The best way to manage your tasks. <br />
            Never forget the cow (or anything else) again!
        </p>

        <p>
            Log in now to manage your to-do list. <br />
            If you do not have an account, one will be created for you.
        </p>

        <form id="loginform" action="login.php" method="post">
            <div><input name="name" type="text" size="8" autofocus="autofocus" /> <strong>User Name</strong></div>
            <div><input name="password" type="password" size="8" /> <strong>Password</strong></div>
            <div><input type="submit" value="Log in" /></div>
        </form>

        <p>
            <!--shows the last login time-->
            <em>(last login from this computer was <?= $_COOKIE["logintime"]?> )</em>
        </p>
    </div>

    <?php bottom();  // gets the common bottom content from common.php
?>