<?php
/**
 * HW8 Jiangzhenjun liao
 * Student#: 1429888
 * CSE 154, TA: Susan Wolfgram
 * This is a PHP service which is the target where start.php submits its
 * login form data to log the user in
 */
    include_once ("common.php"); // implements a shared code file
    hasLoggedIn(); // if the user has already logged in, redirect to todolist.php

    // gets two parameters from start.php
    $password = $_POST["password"];
    $user = $_POST["name"];

    // if either of the parameters is missing
    if (!isset($user) || !isset($password)) {
        directToStart(); // login fails and directs to start.php
    } else { // if both parameters are set
        if (user_exists($user)) { // user exists in the users.txt
            if (check_password($user, $password)) { // password is correct
                logIn($user); // logs in the user
            } else { // wrong password
                directToStart();
            }
        } else { // user does not exists in the file
            if (checkFormat($user, $password)) { // user name and password in correct format
                storeData($user, $password); // stores the user data in the file
                logIn($user);
            } else {
                directToStart();
            }
        }
    }

    // takes in user name and password as parameters,
    // stores the user name and password in users.txt
    function storeData($user, $password) {
        $input = $user . ':' . $password . "\n";
        file_put_contents("users.txt", $input, FILE_APPEND);
    }

    // takes in user name, and checks if user exists
    // returns true if user exists
    function user_exists($user) {
        if (file_exists("users.txt")) { // there is a file
            $files = file("users.txt"); // store each user data in an array
            foreach ($files as $file) { // go through each info
                list($username) = explode(":", $file, 2); // gets the username
                if ($user == $username) { // input user name exists
                    return true; // there has already been an user in the file
                }
            }
        }
        return false; // no such file or no such user
    }

    // takes in user name and password and checks if the password matches the user name
    // returns true if the input password is correct
    function check_password($user, $password){
        // store each user info in an array
        $files = file("users.txt", FILE_IGNORE_NEW_LINES);
        foreach ($files as $file){ // go through each user's info
            $data = explode(":", $file, 2); // seperate user name and their password
            if ($user == $data[0] && $password == $data[1]) { // they match!
                return true;
            }
        }
        return false; // wrong password
    }

    // checks if the input user name and password are in correct format
    // takes in user name and password, returns true if format is correct
    function checkFormat($user, $password) {
        if (preg_match("/^[a-z][a-z0-9]{2,7}$/", $user)
            && preg_match("/^[0-9].{4,11}(?<![0-9a-z])$/", $password)) {
            return true;
        }
        return false;
    }

    // logs in the user, starts session and cookie
    // takes in user name and directs the page to todolist.php
    function logIn($user) {
        session_start(); // starts a session which stores the user's name
        $_SESSION["name"] = $user;
        // sets a cookie stores the login time of the user
        // and will expire 7 days later
        setcookie("logintime", date("D y M d, g:i:s a"), time() + (86400 * 7));
        header("Location: todolist.php"); // directs to the todolist.php
        die();
    }

