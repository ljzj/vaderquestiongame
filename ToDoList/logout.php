<?php
/**
 * HW8 Jiangzhenjun liao
 * Student#: 1429888
 * CSE 154, TA: Susan Wolfgram
 * This is a PHP service which logs the user out
 * and returns them to the front page start.php
 */
    include_once ("common.php"); // implements a shared code file
    notLoggedIn();  // if user has not been logged in, directs to start.php
    session_destroy(); // clear the session
    directToStart(); // directs to start.php

?>