# Project Overview
I create a web page for a fictional online to-do list site called "Remember the Cow", which is a parody of the real to-do list web site "Remember the Milk".

## What did I do?
In this project I write a web application for an online to-do list. The project shows my understanding of user login sessions and cookies, and building a large site with many connected pages. 

## Project Language and Structure
* PHP
* CSS
* Multiple Connected Pages

## How to view it?
Please download all the files on your local machine. Run *start.php* on a server and start writing your to-do list! :books:

My site requires the user to log in or create an account first. After logging in, you can manipulate your to-do list by adding or deleting items. Any changes made to the list are saved to the web server, so that if you leave the page and return later, the state of the list is remembered.

