<?php
/**
 * HW8 Jiangzhenjun liao
 * Student#: 1429888
 * CSE 154, TA: Susan Wolfgram
 * This is a PHP service page which targets todolist.php submits requests
 * to add/delete items from the list
 */

    include_once ("common.php"); // implements a shared code file
    notLoggedIn(); // if user has not been logged in, directs to start.php

    $mode = $_POST["action"]; // stores the post parameter

    if (!isset($mode)) { // if parameter is missing
        die(); // stops immediately
    }

    $fileName = "todo_" . $_SESSION["name"] . ".txt";

    if ($mode == "add") { // if user is adding an item
        $item = $_POST["item"]; // gets the input
        // puts the input contents in the file with a new line
        file_put_contents($fileName, $item . "\n", FILE_APPEND);
        header("Location: todolist.php"); // directs to todolist.php
        die();
    } else { // user is deleting an item
        $index = $_POST["index"]; // gets the index of what is going to be deleted
        if (!preg_match("/^[0-9]+$/", $index)) { // if the index is negative or not a number
            die(); // stops immediately
        }
        // stores items in an array
        $lines = file($fileName);
        $size = sizeof($lines); // gets the size of the array
        if ($index >= $size) { // if the given index is bigger than the size of the array
            die(); // stops immediately
        }
        unset($lines[$index]); // clears the content in given index of the array
        $content = implode(" ", $lines); // combine arrays into one string
        file_put_contents($fileName, $lines); // re-write the file
        header("Location: todolist.php"); // directs to todolist.php
        die();
    }

?>