# Project Overview
This is a mock move-review website that shows movie ratings and reviews. 

## What did I do?
This project is about CSS and HTML for layout, positioning, and the CSS box model. I create files for a fake movie review web site named Rancid Tomatoes for the film TMNT.

## Project Language and Structure
* CSS
* HTML

## How to view it?
Please download all the files on your local machine. Open *tmnt.html* to see what I have created! :turtle: 
