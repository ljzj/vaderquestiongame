/**
 * Assignment6
 * Jiangzhenjun Liao
 * Student#, 1429888
 * CSE 154, TA: Susan Wolfgram
 * This Javascript requests information from bestreads.php and inject it into bestreads.html
 */
(function() {

    "use strict";

    // sets up onclick handlers
    // uses a help method to load books from the web server
    window.onload = function() {
        makeRequest(loadBook, "bestreads.php?mode=books");
        document.getElementById("back").onclick = home;
    };

    // called by the helper method when the XML data of books arrives
    // displays each book's image and title and hides the siglebook div
    // when a single book is clicked, calls the singlepage function
    function loadBook() {
        document.getElementById("singlebook").style.display = "none";
        var response = this.responseXML;
        var title = response.querySelectorAll("title"); // stores all titles in an array
        var folder = response.querySelectorAll("folder"); // stores all folder in an array
        for (var i = 0; i < title.length; i++) { // handles each book
            var div = document.createElement("div");
            var paragraph = document.createElement("p");
            paragraph.innerHTML = title[i].textContent;
            var img = document.createElement("img");
            img.src =  folder[i].textContent + "/cover.jpg";
            img.alt = folder[i].textContent;
            div.appendChild(img);
            div.appendChild(paragraph);
            document.getElementById("allbooks").appendChild(div);
            div.onclick = singlePage; // calls the singlePage function
        }
    }

    // called when a single book on the page is clicked
    // display a single page of this book's information, description, and reviews
    // by calling corresponding methods
    // cleans the previous allbook div
    function singlePage() {
        var title = this.querySelector("img").getAttribute("alt");
        title = title.split("/")[1]; // gets the folder name
        document.getElementById("cover").src = "books/" + title + "/cover.jpg";
        document.getElementById("allbooks").innerHTML = ""; // cleans previous allbooks div
        document.getElementById("singlebook").style.display = "block"; // unhidden singlepage div
        // makes a request to load book information
        makeRequest(loadInfo, "bestreads.php?mode=info&title=" + title );
        // makes a request to load description of the book
        makeRequest(loadDescription, "bestreads.php?mode=description&title=" + title);
        // makes a request to load reviews of the book
        makeRequest(loadReviews, "bestreads.php?mode=reviews&title=" + title);
    }

    // reloads the page when the home button is clicked
    function home() {
        makeRequest(loadBook, "bestreads.php?mode=books");
    }

    // called by the helper method when JSON data of the book information is arrived
    // Inserts title, author and stars gotten from the info request into corresponding html area
    function loadInfo() {
        var response = JSON.parse(this.responseText);
        document.getElementById("title").innerHTML = response.title;
        document.getElementById("author").innerHTML = response.author;
        document.getElementById("stars").innerHTML = response.stars;
    }

    // called by the helper method when text data of the book description is arrived
    // Inserts book description gotten from the info request into corresponding html area
    function loadDescription() {
        document.getElementById("description").innerHTML = this.response;
    }

    // called by the helper method when XML data of the book reviews is arrived
    // Inserts each review from the info request into corresponding html area
    function loadReviews() {
        document.getElementById("reviews").innerHTML = this.response;
    }

    // A helper method which makes request to fetch data from the server
    // takes in a function name and a web server link
    function makeRequest (functionName, link) {
        var request = new XMLHttpRequest();
        request.onload = functionName;
        request.open("GET", link, true);
        request.send();
    }

})();