<?php
/**
 * HW6 Jiangzhenjun liao
 * Student#: 1429888
 * CSE 154, TA: Susan Wolfgram
 * This is a PHP service that will provide the book data for bestreads web page
 */

# Takes two parameters and provides different data
$mode = $_GET["mode"]; // introduces the mode parameter
if ($mode == "books") { // when the mode is "books"
    outputXML();
} else { // when the mode is not "books"
    $book = $_GET["title"]; // introduces the title parameter
    if ($mode == "info") {
        outputJson($book); // takes in the title parameter
    } else if ($mode == "description") {
        outputText($book); // takes in the title parameter
    } else {
        outputHTML($book); // takes in the title parameter
    }
}

# outputs three lines of a book's information as JSON based on the passed in book title
function outputJson($book) {
    // uses list because there are exactly three variables
    // stores title, author, and stars in the list and sets atteibutes in JSON
    list($title, $author, $stars) = file("books/$book/info.txt");
    $json = array("title" => $title, "author" => $author, "stars"=> $stars);
    header("Content-type: application/json");
    print(json_encode($json)); // prints the output
}

# outputs a book's description as plain text based on the passed in book title
function outputText($book){
    // open the description file
    $content = file_get_contents("books/$book/description.txt");
    header("Content-type: text/plain");
    print $content; // prints the description
}

#outputs reviews of a book with reviewer's name and star as HTML based on the passed in book title
function outputHTML($book) {
    $files = glob("books/$book/review*.txt"); // returns an array of all review files
    foreach ($files as $file) { // go through each review file
        list($reviewer, $star, $review) = file($file);
        // put each reviewer, star, and review into certain html tags
        ?><h3><?= $reviewer?><span><?= $star?></span></h3><p><?= $review?></p><?;
    }
    header("Content-type: text/html");
}

# outputs titles and folder names of all books in XML format
function outputXML(){
    $bookfile = glob("books/*"); // returns an array of all book folders
    $dom = new DOMDocument(); // creates a new dom object
    $books = $dom->createElement("books");
    $dom->appendChild($books);
    foreach ($bookfile as $eachBook) { // for each book
        list($fullName) = file("$eachBook/info.txt"); // gets its title
        $book = $dom->createElement("book");
        $title = $dom->createElement("title");
        $folder = $dom->createElement("folder");
        $folder->appendChild($dom->createTextNode("$eachBook")); // folder name
        $title->appendChild($dom->createTextNode("$fullName")); // book title
        $book->appendChild($title);
        $book->appendChild($folder);
        $books->appendChild($book);
    }
    header("Content-type: text/xml");
    print ($dom->saveXML()); // prints XML
}

?>