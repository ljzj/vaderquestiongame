# Project Overview
This is a mock book review website where you can find ratings and readers' reviews of different books. 

## What did I do?
This project is about making a web service and using Ajax to retrieve data from it. I write a PHP service that
will generate a variety of data about different books depending on the parameters it is sent. I also write Javascript code to make requests to this service and inject the responses into a page. 

## Project Language and Structure
* Javasript
* PHP

## How to view it?
Please download all the files and run it on your local machine.
