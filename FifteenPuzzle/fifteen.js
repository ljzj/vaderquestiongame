/**
 * Jiangzhenjun Liao
 * Student ID: 1429888
 * Section: AE
 * Assignment: 4
 * This is a javascript file for fifteen puzzle page (extra feature included);
 * It generates a fifteen puzzle game for users to play with a size of 4x4 grid.
 */

(function() {

    "use strict";

    var emptyX = 300; // original X-coordinate for the empty tile
    var emptyY = 300; // original Y-coordinate for the empty tile
    var SIZE = 4; // size of this puzzle game
    var UNIT = 100; // length unit for each tile

    window.onload = function() {
        // crate 15 div elements representing puzzle pieces,
        // attach background image and number for each piece
        for (var i = 0; i < SIZE; i++) { // column
            for (var j = 0; j < SIZE; j++) { // row
                if ( i * SIZE + j != SIZE * SIZE - 1) { // do not create the last piece
                    var area = document.getElementById("puzzlearea");
                    var tile = document.createElement('div'); // create a div
                    tile.className = "tile"; // give it a class name
                    area.appendChild(tile); // append it
                    tile.style.position = "absolute";
                    tile.style.left = UNIT * j + "px";
                    tile.style.top = UNIT * i + "px";
                    tile.innerHTML = i * SIZE + j + 1;
                    tile.style.backgroundPositionX = -UNIT * j + "px";
                    tile.style.backgroundPositionY = -UNIT * i + "px";
                }
            }
        }
        // attach handler functions for button and mouse movements
        document.getElementById("shufflebutton").onclick = shuffle;
        // go through each tile and attach handler functions for
        // the current one that is being clicked, hovered-in, or hovered-out
        for (var i = 0; i < SIZE * SIZE - 1; i++ ) {
            var allTiles = document.getElementsByClassName("tile");
            allTiles[i].onmousedown = moveTo;
            allTiles[i].onmouseover = hover;
            allTiles[i].onmouseout = removeHover;
        }
    };

    // Randomly rearrange tiles of current puzzle game
    function shuffle() {
        for (var i = 0; i < 1000; i++) { // generates 1000 random movements
            var allTiles = document.getElementsByClassName("tile");
            var neighbors = []; // create an array stores the movable tiles
            for (var j = 0; j < SIZE * SIZE - 1; j++) {
                if (movable(allTiles[j])) {
                    neighbors.push(allTiles[j]); // the tile is movable, stores it
                }
            }
            // randomly pick one tile from the movable array and move it
            move(neighbors[Math.floor(Math.random()* neighbors.length)]);
        }
    }

    // Check if a tile is movable(is directly up, down, right, or left of a empty square)
    // passed in the currently given square
    function movable(current) {
        // get the current tile location
        var currentX = parseInt(current.style.left);
        var currentY = parseInt(current.style.top);
        if (currentX == emptyX) {
            // current tile is at the same column with the empty square
            if (emptyY + UNIT == currentY || emptyY - UNIT == currentY) {
                // current tile is at the top or at the bottom of the empty square
                return true;
            } else {
                // current tile is not the neighbor of the empty square
                return false;
            }
        } else if (currentY == emptyY){
            // current tile is at the same row with the empty square
            if (emptyX + UNIT == currentX || emptyX - UNIT == currentX) {
                // current tile is at the right or at the left of the empty square
                return true;
            } else {
                // current tile is not the neighbor of the empty square
                return false;
            }
        } else {
            return false;
        }
    }

    // reset the tile color, border color, and cursor when mouse is no longer hovering
    function removeHover() {
        this.style.color = "black";
        this.style.borderColor = "black";
        this.style.cursor = "default";
    }

    // change the tile color, border color, and cursor when mouse is hovering
    function hover() {
        if(movable(this)) { // tile is movable
            this.style.color = "red";
            this.style.borderColor = "red";
            this.style.cursor = "pointer";
        } else { // tile is not movable
            this.style.cursor = "default";
        }
    }

    // Move a tile to empty space
    function moveTo() {
        // passed in the currently given tile and move it to empty space
        move(this);
    }

    // move the given tile to the empty space if it is movable
    function move(tile) {
        if (movable(tile)) {
            // it is near to an empty space
            var transX = parseInt(tile.style.left); // store the current tile position
            var transY = parseInt(tile.style.top);
            tile.style.left = emptyX + "px"; // move the tile to the empty space
            tile.style.top = emptyY + "px";
            emptyX = transX; // update the position of the empty space
            emptyY = transY;
        }
        if (solved()) { // extra feature #1!!!
            // the puzzle is solved with every tile back to original position
            // gives a winning notification
            document.getElementById("output").innerHTML = "YOU WIN";
        } else { // puzzle not solved
            document.getElementById("output").innerHTML = "";
        }
    }

    // extra feature, check if the puzzle is solved
    function solved() {
        var allTiles = document.getElementsByClassName("tile");
        for (var i = 0; i < SIZE * SIZE - 1; i++) { // check every tile's current location
            if (parseInt(allTiles[i].style.left) !== UNIT * (i % SIZE) ||
                parseInt(allTiles[i].style.top) !== UNIT * parseInt(i / SIZE)) {
                return false;
            }
        }
        return true;
    }

})();