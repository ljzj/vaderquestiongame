# Project Overview
The Fifteen Puzzle (also called the Sliding Puzzle) is a simple classic game consisting of a 4x4 grid of numbered squares with one square missing. The object of the game is to arrange the tiles into numerical order by repeatedly sliding a square that neighbors the missing square into its empty space. To play this game, users can shuffle the grids and solve them through clicking on individual grids. 

## What did I do?
This project is about JavaScript's Document Object Model (DOM) and events. I  write the CSS and JavaScript code for a page fifteen.html that plays the Fifteen Puzzle. 

## Project Language and Structure
* CSS
* Javascript

## How to play it?
Please download all the files on your local machine. Open *fifteen.html* and start playing! Please enjoy it! 
